###Name: 
Your first and last name.

###Email: 
Your email address.

###Description: 
A short description of your solution.

###Architecture diagram (optional):
A diagram of your design [example.](https://ars.els-cdn.com/content/image/3-s2.0-B9780128053942000155-f15-02-9780128053942.jpg)

###Project setup:
How to run your project.

###Assumptions (optional): 
List of assumptions if you had to take any.

###Improvements (optional): 
What would you have added if you had more time.
